from xml.etree.ElementTree import XML, fromstring

from enum import Enum
class Bow(Enum):
    UP = 1
    DOWN = 2
    NONE = 3


def get_number_of_instruments(root):
    return len(root.find('part-list').findall('score-part'))

def get_number_of_measures(root):
    return len(root.findall('part')[0].findall('measure'))

def get_guitar_instrument(instruments_count):
    string = f"""
            <score-part id="P{instruments_count+1}">
              <part-name>Electric Guitar</part-name>
              <part-abbreviation>El. Guit.</part-abbreviation>
              <score-instrument id="P{instruments_count+1}-I1">
                <instrument-name>Electric Guitar (Treble Clef)</instrument-name>
                </score-instrument>
              <midi-device id="P{instruments_count+1}-I1" port="1"></midi-device>
              <midi-instrument id="P{instruments_count+1}-I1">
                <midi-channel>3</midi-channel>
                <midi-program>28</midi-program>
                <volume>78.7402</volume>
                <pan>0</pan>
                </midi-instrument>
            </score-part>
            """
    return fromstring(string)

def get_guitar_tablature(instruments_count):
    string = f"""
        <score-part id="P{instruments_count+1}">
          <part-name>Electric Guitar</part-name>
          <part-abbreviation>El. Guit.</part-abbreviation>
          <score-instrument id="P{instruments_count+1}-I1">
            <instrument-name>Electric Guitar (Tablature)</instrument-name>
            </score-instrument>
          <midi-device id="P{instruments_count+1}-I1" port="1"></midi-device>
          <midi-instrument id="P{instruments_count+1}-I1">
            <midi-channel>4</midi-channel>
            <midi-program>28</midi-program>
            <volume>78.7402</volume>
            <pan>0</pan>
            </midi-instrument>
        </score-part>
        """
    return fromstring(string)

def get_guitar_attributes():
    string = """
        <attributes>
            <divisions>1</divisions>
            <key>
                <fifths>0</fifths>
            </key>
            <time>
                <beats>4</beats>
                <beat-type>4</beat-type>
            </time>
            <clef>
                <sign>G</sign>
                <line>2</line>
                <clef-octave-change>-1</clef-octave-change>
            </clef>
        </attributes>
    """
    return fromstring(string)

def get_guitar_print():
    string = """
        <print>
            <staff-layout number="1">
                <staff-distance>65.00</staff-distance>
            </staff-layout>
        </print>
    """
    return fromstring(string)

def get_guitar_tablature_attributes():
    string = """
        <attributes>
        <divisions>1</divisions>
        <key>
          <fifths>0</fifths>
          </key>
        <time>
          <beats>4</beats>
          <beat-type>4</beat-type>
          </time>
        <clef>
          <sign>TAB</sign>
          <line>5</line>
          </clef>
        <staff-details>
          <staff-lines>6</staff-lines>
          <staff-tuning line="1">
            <tuning-step>E</tuning-step>
            <tuning-octave>2</tuning-octave>
            </staff-tuning>
          <staff-tuning line="2">
            <tuning-step>A</tuning-step>
            <tuning-octave>2</tuning-octave>
            </staff-tuning>
          <staff-tuning line="3">
            <tuning-step>D</tuning-step>
            <tuning-octave>3</tuning-octave>
            </staff-tuning>
          <staff-tuning line="4">
            <tuning-step>G</tuning-step>
            <tuning-octave>3</tuning-octave>
            </staff-tuning>
          <staff-tuning line="5">
            <tuning-step>B</tuning-step>
            <tuning-octave>3</tuning-octave>
            </staff-tuning>
          <staff-tuning line="6">
            <tuning-step>E</tuning-step>
            <tuning-octave>4</tuning-octave>
            </staff-tuning>
          </staff-details>
        </attributes>
    """
    return fromstring(string)

def get_empty_note(duration):
    string = f"""
                <note>
                    <rest measure="yes"/>
                    <duration>{duration}</duration>
                    <voice>1</voice>
                </note>
            """
    return fromstring(string)

def get_note(duration, note, octave, type):
    string = f"""
        <note>
            <pitch>
                <step>{note}</step>
                <octave>{octave}</octave>
            </pitch>
            <duration>{duration}</duration>
            <voice>1</voice>
            <type>{type}</type>
            <stem>up</stem>
        </note>
    """
    return fromstring(string)

def get_guitar_tablature_harmoney(chord, kind, kind_text):
    string = f"""
        <harmony print-frame="no">
            <root>
              <root-step>{chord}</root-step>
            </root>
            <kind text="maj">major</kind>
    """
    if kind != "":
        string += f'<kind text="{kind}">{kind_text}</kind>'
    string += '</harmony>'

    return fromstring(string)

def get_guitar_tablature_note(note, octave, duration, type, stem, string, fret, bow):

    s = f"""
        <note>
            <pitch>
              <step>{note}</step>
              <octave>{octave}</octave>
            </pitch>
            <duration>{duration}</duration>
            <voice>1</voice>
            <type>{type}</type>
            <stem>{stem}</stem>
            <notations>
              <technical>
    """

    if bow == Bow.UP:
        s += "<up-bow/>"
    elif bow == Bow.DOWN:
        s += "<down-bow/>"

    s += f"""
        <string>{string}</string>
                <fret>{fret}</fret>
                </technical>
            </notations>
      </note>
    """

    return fromstring(s)

def pretty_xml(element, indent, newline, level=0):  # Elemnt is passed in Elment class parameters for indentation indent, for wrapping NEWLINE
    if element:  # Determine whether the element has child elements
        if (element.text is None) or element.text.isspace():  # If there is no element of text content
            element.text = newline + indent * (level + 1)
        else:
            element.text = newline + indent * (level + 1) + element.text.strip() + newline + indent * (level + 1)
            # Else: # here two lines if the Notes removed, Element will start a new line of text
            # element.text = newline + indent * (level + 1) + element.text.strip() + newline + indent * level
    temp = list(element)  # Element will turn into a list
    for subelement in temp:
        if temp.index(subelement) < (len(temp) - 1):  # If it is not the last element of the list, indicating that the next line is the starting level of the same elements, indentation should be consistent
            subelement.tail = newline + indent * (level + 1)
        else:  # If it is the last element of the list, indicating that the next line is the end of the parent element, a small indentation should
            subelement.tail = newline + indent * level
        pretty_xml(subelement, indent, newline, level=level + 1)  # Sub-elements recursion
