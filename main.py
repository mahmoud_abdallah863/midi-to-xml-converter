import xml.etree.ElementTree as ET
from Functions import get_number_of_instruments, get_guitar_instrument,\
    get_guitar_tablature, pretty_xml, get_guitar_print, get_guitar_attributes, get_guitar_tablature_attributes,\
    get_empty_note, get_number_of_measures, get_note, get_guitar_tablature_harmoney, get_guitar_tablature_note

from Functions import Bow

def readXmlFile(file_location):
    mytree = ET.parse(file_location)
    root = mytree.getroot()
    return mytree, root


def add_guitar_instrument(root):
    number_of_instruments = get_number_of_instruments(root)
    part_list = root.find('part-list')
    guitar_instrument = get_guitar_instrument(number_of_instruments)
    part_list.append(guitar_instrument)

def add_measures_to_guitar_instrument(root):
    number_of_instruments = get_number_of_instruments(root)

    new_part = ET.SubElement(root, 'part')
    new_part.set('id', f'P{number_of_instruments}')

    new_measure = ET.SubElement(new_part, 'measure')
    new_measure.set('number', '1')
    new_measure.set('width', '1')

    new_measure.append(get_guitar_print())
    new_measure.append(get_guitar_attributes())

    new_measure.append(get_empty_note(4))

    for i in range(1, get_number_of_measures(root)):
        new_measure = ET.SubElement(new_part, 'measure')
        new_measure.set('number', f'{i + 1}')
        new_measure.set('width', '1')

        new_measure.append(get_empty_note(4))


def add_guitar_tablature(root):
    number_of_instruments = get_number_of_instruments(root)
    part_list = root.find('part-list')
    guitar_tablature = get_guitar_tablature(number_of_instruments)
    part_list.append(guitar_tablature)


def add_measures_to_guitar_tablature(root):
    number_of_instruments = get_number_of_instruments(root)

    new_part = ET.SubElement(root, 'part')
    new_part.set('id', f'P{number_of_instruments}')

    new_measure = ET.SubElement(new_part, 'measure')
    new_measure.set('number', '1')
    new_measure.set('width', '1')

    new_measure.append(get_guitar_print())
    new_measure.append(get_guitar_tablature_attributes())

    new_measure.append(get_empty_note(4))

    chords = ['C', 'C', 'C', 'F', 'C', 'F', 'C', 'C', 'C']

    for i in range(1, get_number_of_measures(root)):
        new_measure = ET.SubElement(new_part, 'measure')
        new_measure.set('number', f'{i+1}')
        new_measure.set('width', '1')

        new_measure.append(get_guitar_tablature_harmoney(chords[i-1], '', ''))

        new_measure.append(get_guitar_tablature_note('D', '3', '1', 'quarter', 'none', '4', '0', Bow.DOWN))
        new_measure.append(get_guitar_tablature_note('D', '3', '1', 'quarter', 'none', '4', '0', Bow.NONE))
        new_measure.append(get_guitar_tablature_note('D', '3', '1', 'quarter', 'none', '4', '0', Bow.UP))
        new_measure.append(get_guitar_tablature_note('D', '3', '1', 'quarter', 'none', '4', '0', Bow.NONE))




if __name__ == '__main__':
    (mytree, root) = readXmlFile('xml/youAreMySunshine.xml')

    add_guitar_instrument(root)
    add_measures_to_guitar_instrument(root)

    add_guitar_tablature(root)
    add_measures_to_guitar_tablature(root)

    number_of_instruments = get_number_of_instruments(root)
    print(f'number = {number_of_instruments}')


    pretty_xml(root, '\t', '\n')
    mytree.write('xml/Testing.xml')


